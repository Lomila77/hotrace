/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/05 15:16:50 by gcolomer          #+#    #+#             */
/*   Updated: 2021/04/05 15:16:50 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "hash.h"
#include "struct.h"
#include "slot.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main (void)
{
	t_data	**tab;
	t_data	*slot;
	char	*line = "!bonjour";
	FILE	*fd;
	int		len;

	fd = 0;
	tab = create_tab(&len);
//	while (gets(line) > 0)
	while (1)
	{
		if (strchr((const char *)line, '=') != NULL)
		{
			slot = init_struct(line);
			add(tab, slot, len);
		}
		else if (line[0] == '!') // c'est qu'on souhaite supprimer le mot cle
		{
			remove_data(tab, line, len);
		}
		else // c'est qu'on rentre un mot cles
		{
			search(tab, line, len);
		}
		break ;
	}
//	clean(tab); // fonction permettant de corriger toutes les leaks
	free(line);
}
