/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hash.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/05 15:18:56 by gcolomer          #+#    #+#             */
/*   Updated: 2021/04/05 15:18:56 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "hash.h"
#include "stdlib.h"
#include <string.h>

void	clean(t_data *data)
{
	(void)data;
	return ;
}

t_data	**create_tab(int *len)
{
	static int	pow = 2048;
	t_data	**tab;

	if (pow != 2048)
		pow *= pow;
	tab = calloc(pow, sizeof(t_data));
	if (tab == NULL)
		return (NULL);
	*len = pow;
	return (tab);
}

int		hash_func(char *key, int len)
{
	t_cook			hash;
	int				a;
	int				b;
	int				c;
	unsigned int	res;

	a = 24;
	b = 9043;
	c = 1565;
	strlcpy(hash.temp, key, 8);
	res = (hash.content[0] * a + hash.content[1] * b + c) % len;
	return (res);
}

int		comp_hash(t_data **tab, char *key, int len)
{
	int	i;
	char cmp[8];

	strlcpy(cmp, key, 8);
	i = hash_func(key, len);
	while (i < len)
	{
		if ()
		if (strcmp(tab[i]->hash, cmp) == 0)
		{
			if (strcmp(tab[i]->key, key) == 0)
				return (i);
		}
		else
			i++;
	}
	return (-1);
}
