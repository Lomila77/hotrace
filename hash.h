/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hash.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/05 15:19:31 by gcolomer          #+#    #+#             */
/*   Updated: 2021/04/05 15:19:31 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HASH_H
# define HASH_H

typedef struct
{
	char	*key;
	char	*value;
	char	hash[8];
}			t_data;

typedef union
{
	char			temp[8];
	unsigned int	content[2];
}			t_cook;

t_data	**create_tab(int *len);
int		hash_func(char *key, int len);
int		comp_hash(t_data **tab, char *key, int len);

#endif
